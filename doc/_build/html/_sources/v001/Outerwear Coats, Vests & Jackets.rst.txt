Outerwear Coats, Vests & Jackets
================================
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Sleeve Length
^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments


Jacket Style
^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7084

Outerwear Type
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments


Suit Jacket Style
^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments


