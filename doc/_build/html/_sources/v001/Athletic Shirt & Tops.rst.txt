Athletic Shirt & Tops
=====================
Pattern
^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Athletic Clothes, Shoes & Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Collar Type
^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Athletic Clothes, Shoes & Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6268

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Athletic Clothes, Shoes & Accessories


Sleeve Length
^^^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Athletic Clothes, Shoes & Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381

Color Category
^^^^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Athletic Clothes, Shoes & Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

