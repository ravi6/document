Sweatshirt & Hoodies
====================
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Is Hooded
^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6145

Sleeve Length
^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381

Sweatshirt & Hoodie Style
^^^^^^^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Upper-Body Garments


