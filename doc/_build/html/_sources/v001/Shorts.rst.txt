Shorts
======
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments


Shorts Type
^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7106

