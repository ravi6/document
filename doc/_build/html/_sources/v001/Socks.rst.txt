Socks
=====
Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Underwear, Nightwear & Lingerie
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Underwear, Nightwear & Lingerie


Sock Length
^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Underwear, Nightwear & Lingerie


Sock Style
^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Underwear, Nightwear & Lingerie
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7540

