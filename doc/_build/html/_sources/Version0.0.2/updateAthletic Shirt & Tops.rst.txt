Athletic Shirt & Tops
=====================
Category : Sports & Outdoors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sub Category : Athletic Clothes, Shoes & Accessories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Pattern
^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30
| Updated : Yes
| Update note : txt


Collar Type
^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6268
| Updated : Yes
| Update note : txt


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Updated : Yes
| Update note : txt


Sleeve Length
^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381
| Updated : Yes
| Update note : txt


Color Category
^^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081
| Updated : Yes
| Update note : txt


