Eyeglasses
==========
Frame Type
^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Worn Fashion Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/965

Frame Style
^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Worn Fashion Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7569

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Worn Fashion Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Worn Fashion Accessories


