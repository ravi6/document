Eyeglasses
==========
Category : Clothing, Shoes & Accessories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sub Category : Worn Fashion Accessories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Frame Type
^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/965
| Updated : Yes
| Update note : txt


Frame Style
^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7569
| Updated : Yes
| Update note : txt


Color Category
^^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081
| Updated : Yes
| Update note : txt


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Updated : Yes
| Update note : txt


