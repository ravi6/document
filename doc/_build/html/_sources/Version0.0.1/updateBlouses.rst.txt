Blouses
=======
Category : Clothing, Shoes & Accessories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sub Category : Upper-Body Garments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Pattern
^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30
| Updated : Yes
| Update note : txt


Color Category
^^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081
| Updated : Yes
| Update note : txt


Is Off Shoulder
^^^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6276
| Updated : Yes
| Update note : txt


Dress Occasion
^^^^^^^^^^^^^^^^
| Updated : Yes
| Update note : txt


Collar Type
^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6268
| Updated : Yes
| Update note : txt


Sleeve Length
^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381
| Updated : Yes
| Update note : txt


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Updated : Yes
| Update note : txt


