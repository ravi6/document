Sandals
=======
Category : Clothing, Shoes & Accessories
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Sub Category : Footwear
^^^^^^^^^^^^^^^^^^^^^^^^
Color Category
^^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081
| Updated : Yes
| Update note : txt


Shoe Category
^^^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6348
| Updated : Yes
| Update note : txt


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Updated : Yes
| Update note : txt


Sandal Type
^^^^^^^^^^^^^
| Taxanomy Link :https://taxonomy.datax.ai/attributes/7305

