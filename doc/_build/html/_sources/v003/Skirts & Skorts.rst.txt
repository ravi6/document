Skirts & Skorts
===============
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Dress & Skirt Length
^^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments


Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Lower-Body Garments


