Sports Bra
==========
Pattern
^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Underwear, Nightwear & Lingerie
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Bra Type
^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Underwear, Nightwear & Lingerie
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6389

Color Category
^^^^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Underwear, Nightwear & Lingerie
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Sports & Outdoors
| Sub Category : Underwear, Nightwear & Lingerie


