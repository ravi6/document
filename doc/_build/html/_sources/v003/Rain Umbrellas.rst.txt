Rain Umbrellas
==============
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Carried Fashion Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Carried Fashion Accessories
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Carried Fashion Accessories


