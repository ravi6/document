Dresses
=======
Pattern
^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits
| Taxanomy Link :https://taxonomy.datax.ai/attributes/30

Color Category
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits
| Taxanomy Link :https://taxonomy.datax.ai/attributes/1081

Dress & Skirt Length
^^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits


Dress Occasion
^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits


Is Off Shoulder
^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6276

Color Actual Shades
^^^^^^^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits


Collar Type
^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits
| Taxanomy Link :https://taxonomy.datax.ai/attributes/6268

Sleeve Length
^^^^^^^^^^^^^^^
| Category : Clothing, Shoes & Accessories
| Sub Category : Full-Body Garments & Suits
| Taxanomy Link :https://taxonomy.datax.ai/attributes/12381

