Handbags
========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Carried Fashion Accessories
-------------------------------------------
.. csv-table::
   :file: updatedHandbags.csv
   :header-rows: 1