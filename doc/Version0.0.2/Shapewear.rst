Shapewear
=========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Underwear, Nightwear & Lingerie
-----------------------------------------------
.. csv-table::
   :file: Shapewear.csv
   :header-rows: 1