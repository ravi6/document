Blouses
=======
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Upper-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedBlouses.csv
   :header-rows: 1