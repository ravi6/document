Shorts
======
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedShorts.csv
   :header-rows: 1