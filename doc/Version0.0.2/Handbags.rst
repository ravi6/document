Handbags
========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Carried Fashion Accessories
-------------------------------------------
.. csv-table::
   :file: Handbags.csv
   :header-rows: 1