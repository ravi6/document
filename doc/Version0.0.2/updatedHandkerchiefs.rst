Handkerchiefs
=============
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Carried Fashion Accessories
-------------------------------------------
.. csv-table::
   :file: updatedHandkerchiefs.csv
   :header-rows: 1