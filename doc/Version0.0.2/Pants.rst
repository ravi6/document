Pants
=====
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: Pants.csv
   :header-rows: 1