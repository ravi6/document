Socks
=====
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Underwear, Nightwear & Lingerie
-----------------------------------------------
.. csv-table::
   :file: Socks.csv
   :header-rows: 1