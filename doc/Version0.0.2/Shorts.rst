Shorts
======
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: Shorts.csv
   :header-rows: 1