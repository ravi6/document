Skirts & Skorts
===============
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedSkirts & Skorts.csv
   :header-rows: 1