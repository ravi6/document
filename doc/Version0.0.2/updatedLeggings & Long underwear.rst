Leggings & Long underwear
=========================
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedLeggings & Long underwear.csv
   :header-rows: 1