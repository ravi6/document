Boots
=====
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Footwear
------------------------
.. csv-table::
   :file: Boots.csv
   :header-rows: 1