Suits
=====
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Upper-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedSuits.csv
   :header-rows: 1