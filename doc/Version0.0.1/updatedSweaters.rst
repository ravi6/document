Sweaters
========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Upper-Body Garments
-----------------------------------
.. csv-table::
   :file: updatedSweaters.csv
   :header-rows: 1