Sweatpants & Pajamas
====================
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Underwear, Nightwear & Lingerie
-----------------------------------------------
.. csv-table::
   :file: Sweatpants & Pajamas.csv
   :header-rows: 1