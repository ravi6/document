Skirts & Skorts
===============
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Lower-Body Garments
-----------------------------------
.. csv-table::
   :file: Skirts & Skorts.csv
   :header-rows: 1