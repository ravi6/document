Sweaters
========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Upper-Body Garments
-----------------------------------
.. csv-table::
   :file: Sweaters.csv
   :header-rows: 1