Bags
====
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Carried Fashion Accessories
-------------------------------------------
.. csv-table::
   :file: Bags.csv
   :header-rows: 1