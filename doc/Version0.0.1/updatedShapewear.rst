Shapewear
=========
Category : Clothing, Shoes & Accessories
-----------------------------------------
Sub Category : Underwear, Nightwear & Lingerie
-----------------------------------------------
.. csv-table::
   :file: updatedShapewear.csv
   :header-rows: 1